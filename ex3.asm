.data
pedir_int: .asciiz "Coloque um n�mero inteiro \n"
mul_str: .asciiz " x "
equal_str: .asciiz " = "
debugger: .asciiz "\nDebugger : \n" 
numero1_str: .asciiz "O factorial de 1 � 1" 

.text
	#Pede um n�mero
	li $v0, 4
	la $a0, pedir_int 
	syscall
	
	#Guarda o n�mero em v0 
	li $v0, 5 
	syscall 
	move $t1, $v0 #t1 tem o n�mero para fazer o for 
	
	beq $t1, 1, numero1 #se o n�mero for 1 s� imprime string 
	
	#Criar um contador 
	li $t2, 0 #variavel contador
	#Criar total 
	li $t3, 1 #variavel total 

	bne $t2, $t1, for #sen�o, enquanto o contador n�o for igual ao n�mero, faz um for 
	#Ciclo For 
	for:
	addi $t2, $t2,  1 #contador + 1 
	beq $t1, $t2, calcular_factorial
	
	li $v0, 1 #imprimir o n�mero atual
	move $a0, $t2 #mover o valor para a0 
	syscall
	
	mul $t3, $t3, $t2 #multiplico o n�mero atual x pr�ximo 
		
	li $v0, 4 #imprimir o x
	la $a0, mul_str 
	syscall
	
	b for 
	
	calcular_factorial:
	li $v0, 1 #imprimir o n�mero atual
	move $a0, $t1 #mover o valor para a0 
	syscall
	#multiplico o total at� n-1 com o n 
	mul $t4, $t3, $t1 
	q
	li $v0, 4 
	la $a0, equal_str 
	syscall 
	
	li $v0, 1
	move $a0, $t4
	syscall  
	b end
	
	numero1:
	li $v0, 4
	la $a0, numero1_str
	syscall 
	b end
	
	end:
#	li $v0, 4 
#	la $a0, debugger
#	syscall
#	li $v0, 1
#	move $a0, $t3
#	syscall
