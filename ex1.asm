.data #dados
frase: .byte 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64, 0x00
pedir_int: .asciiz "Introduza um n�mero inteiro \n"
soma: .asciiz "A soma �: \n"

.text #programa a ser executado
main:
	## 1. Imprimir HelloWorld com byte ##
	# li $v0, 4 #primeiro argumento do syscall - imprimir string
	# la $a0, frase #segundo argumento - dados
	#syscall
	
	## 2. Pedir n�mero, somar e imprimir ##
	#Imprimir texto para pedir n�mero
	#li $v0, 4
	#la $a0, pedir_int 
	#syscall
	#Ler o n�mero
	#li $v0, 5
	#syscall 
	#Guardar o valor em t1
	#move $t1, $v0
	#Imprimir texto para pedir n�mero
	#li $v0, 4
	#la $a0, pedir_int 
	#syscall
	#li $v0, 5
	#syscall
	#Guardar o valor em t2
	#move $t2, $v0
	#Fazer adi��o
	#add $v1, $t2, $t1
	#Imprimir o resultado
	#Imprimir texto para pedir n�mero
	#li $v0, 4
	#la $a0, soma 
	#syscall
	#li $v0, 1 #Instru��o 1 = Imprimir Int 
  	#move $a0, $v1
	#syscall
	
	## 3. Pedir string, imprimir invertida
	li $a0, 20
	li $v0, 9
	syscall
	li $v0, 8
	syscall 
	move $t1, $v0
	li $v0, 4
	move $a0, $t1
	li $a1, 20  
	syscall
	

