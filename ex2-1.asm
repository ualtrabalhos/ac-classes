.data: 
pedir_int: .asciiz "Introduza um n�mero \n"
erro_str: .asciiz "Introduziu um n�mero menor ou igual a 0"
par_str: .asciiz "O n�mero � par"
impar_str: .asciiz "O n�mero � impar" 
end_str: .asciiz "\nAcabou o programa"

.text:
main:
	li $v0, 4 
	la $a0, pedir_int
	syscall
	li $v0, 5 
	syscall 
	move $t1, $v0 
	
	blez $t1, erro
	
	andi $t2, $t1, 1
	bgtz $t2, impar	
	
	par: 
	li $v0, 4
	la $a0, par_str
	syscall
	b end 
	
	impar:
	li $v0, 4
	la $a0, impar_str 
	syscall
	b end 
		
	erro:
	li $v0, 4 
	la $a0, erro_str
	syscall 
	
	end: 
	li $v0, 4
	la $a0, end_str
	syscall 