# Arquitectura de Computadres - Geral
## Repo para testes e exercicios da aula.

**Como utilizar?**

1. Instalar git -> https://git-scm.com/downloads
2. `git clone git@bitbucket.org:ualtrabalhos/sistemas-operativos-geral.git`
3. Executar programa. Instalar MARS.

**Exercí­cios**

1. Escrever HelloWorld, ler e imprimir inteiro
